<?php
/**
 * @package facilitec
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?php
    global $page, $paged;

    wp_title( '|', true, 'right' );

    // Add the blog name.
    bloginfo( 'name' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description";

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( __( 'Page %s', 'facilitec' ), max( $paged, $page ) );

    ?></title>
    
    <meta name="author" content="Marius Stuparu http://www.mariusstuparu.com">
    <meta name="viewport" content="width=device-width">
    
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/main.css">
    <?php if( is_home() ) : ?>
    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/default/default.css">
    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/nivo-slider.css">
	<?php endif; ?>
    
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo( 'rss2_url' ); ?>" />
    <link rel="alternate" type="application/rss+xml" title="Atom" href="<?php bloginfo( 'atom_url' ); ?>" />
    
    <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico" />

    <script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!--[if lte IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header>
        	<div class="content">
        		<div id="header_top" class="clearfix">
        			<h1 id="logo">
        				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
        			</h1>
        			
        			<div id="social">
        				<?php 
        				if( function_exists('ot_get_option') ) {
        					$facebook = ot_get_option( 'social_facebook', 'http://www.facebook.com' );
        					$linkedin = ot_get_option( 'social_linkedin', 'http://www.linkedin.com' );
							$pinterest = ot_get_option( 'social_pinterest', 'http://www.pinterest.com' );
						} else {
							$facebook = 'http://www.facebook.com';
        					$linkedin = 'http://www.linkedin.com';
							$pinterest = 'http://www.pinterest.com';
						}
        				?>
        				<a href="<?php echo $facebook ?>" class="facebook">facebook</a>
        				<a href="<?php echo $linkedin ?>" class="linkedin">linkedin</a>
        				<a href="<?php echo $pinterest ?>" class="pinterest">pinterest</a>
        			</div>
        		</div>
        		
        		<div id="heading">
        			<h2>Facility &amp; Property Management<br />for <a href="#">offices</a> and <a href="#">residential</a> spaces.</h2>
        			<div class="contact">
        				<a href="<?php echo get_page_uri(2) ?>">contact us now</a>
        			</div>
        		</div>
        	</div>
        </header>
        
        <div id="nav">
            <div class="content">
                <div id="f_icon_small"></div>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'container' => 'nav',
                        'container_id' => 'main_navigation'
                    ) );
                ?>
            </div>
        </div>