var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

var stylez = [
  {
    featureType: "road",
    elementType: "all",
    stylers: [
      { hue: "#00ddff" },
      { saturation: -100 },
      { invert_lightness: false },
      { lightness: -35 }
    ]
  },{
    featureType: "landscape",
    elementType: "all",
    stylers: [
      { hue: "#bababa" },
      { saturation: -99 },
      { lightness: -25 },
            { visibility: "off" }
    ]
  },{
    featureType: "poi",
    elementType: "all",
    stylers: [
      { invert_lightness: true },
      { hue: "#004cff" },
      { saturation: -100 },
      { lightness: 52 }
    ]
  },{
        featureType: "landscape.man_made",
    elementType: "geometry",
    stylers: [
      { visibility: "off" }
    ]
    }
]

function mapInitialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var adcentric = new google.maps.LatLng(latitude,longitude);
    var myOptions = {
        zoom:7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(latitude,longitude),
        zoom: 17,
        scrollwheel: false,
        streetViewControl:false,
        mapTypeControl: false,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setOptions( { suppressMarkers: true } );
    directionsDisplay.setMap(map);
    var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });
        map.mapTypes.set('tehgrayz', mapType);
        map.setMapTypeId('tehgrayz');
    var companyImage = new google.maps.MarkerImage('../wp-content/themes/facilitec/img/logom.png');
    var companyPos = new google.maps.LatLng(latitude,longitude);
    var companyMarker = new google.maps.Marker({
            position: companyPos,
            map: map,
            icon: companyImage,
            title:"Adcentric",
            zIndex: 3});
}

function calcRoute() {
    var start = document.getElementById("inputbox").value;
    var end = new google.maps.LatLng(latitude,longitude);
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });
}