jQuery(function($) {
    var setupStickyMenu = function() {
        $.waypoints.settings.scrollThrottle = 30;
        $('#nav').waypoint(function(event, direction) {
            $(this).toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    };
    
    var setupSlider = function() {
        $('#slider').nivoSlider({
            directionNav : false
        });
    };
    
    var enableGoogleMaps = function(){
        yepnope([{
            load: ['../wp-content/themes/facilitec/js/mapps-load.js'],
            complete: function() {
                mapInitialize()
            }
        }])
    }
    
    var Init = function() {
        var $body = $('body')
        
        setupStickyMenu();
        
        if ($body.hasClass('home')) {
            setupSlider();
        }
        
        if ( $body.hasClass('single') ) { // properties
            enableGoogleMaps()
        }
        
        if ( $body.hasClass('page-id-2') ) { // contact
            enableGoogleMaps()
        }
    };
    Init();
})
