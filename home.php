<?php
/**
 * The main template file.
 *
 * @package facilitec
 */

get_header();

$args_slider = array(
    'post_type' => 'post',
    'posts_per_page' => 5,
    'orderby' => 'date',
    'category__in' => array(1, 3) // Select posts from Residential and Office spaces
);

$slider = new WP_Query($args_slider);
?>
<div class="slider-wrapper theme-default">
	<div id="slider" class="nivoSlider">
<?php
$counter = 0;
$slides = array();
$locations = array();
$prices = array();
$links = array();
$names = array();

while ( $slider->have_posts() ) : $slider->the_post();
    $image = get_field('main_image');
    
    if( isset($image) ) :
		
		$locations[] = get_field( 'location' );
		$prices[] = get_field( 'price' );
		$slides[] = $counter;
		$links[] = get_permalink();
		$names[] = get_the_title();
		?>
	
		<a href="<?php the_permalink() ?>"><img src="<?php echo $image ?>" alt="" title="#details<?php echo $counter ?>" /></a>
			
<?php			
		$counter++;
	endif;
endwhile;
?>
	</div> <!-- #slider -->

<?php foreach($slides as $index) : ?>
	<div id="details<?php echo $index ?>" class="nivo-html-caption">
		<h3>
			<span class="name"><?php echo $names[$index] ?></span>
			<span class="location"><?php echo $locations[$index] ?></span>
		</h3>
		<p class="price"><?php echo $prices[$index] ?></p>
		<p class="link"><a href="<?php echo $links[$index] ?>">more info<span class="arrow"></span></a></p>
	</div>
<?php endforeach; ?>
</div> <!-- .slider-wrapper -->

<div id="sections">
	<div class="content">
		<article class="clearfix">
			<h1><?php
				if( function_exists('ot_get_option') ) {
					echo ot_get_option( 'home_heading', 'What can we help you with?' );
				} else {
					echo 'What can we help you with?';
				}
			?></h1>
			
			<section>
			    <a href="<?php echo get_page_uri(7); ?>"><span class="icon icon_services"></span><span class="description">Services</span></a>
			</section>
			
			<section>
                <a href="<?php echo get_category_link(1); ?>"><span class="icon icon_apartments"></span><span class="description">Apartments</span></a>
            </section>
            
            <section>
                <a href="<?php echo get_category_link(3); ?>"><span class="icon icon_office"></span><span class="description">Office Spaces</span></a>
            </section>
		</article>
	</div>
</div>

<?php get_footer(); ?>