<article>
	<div class="content">
		<section class="numbers clearfix">
			<h1>
				<?php echo get_field( 'name' )?>
				<br /><small><?php echo get_field( 'location' )?></small>
			</h1>
			<p class="figures">
				<span class="surface"><i class="icon surface"></i><?php echo get_field( 'surface' )?> m<sup>2</sup></span>
				<span class="beds"><i class="icon beds"></i><?php echo get_field( 'bedrooms' )?> Beds</span>
				<span class="baths"><i class="icon baths"></i><?php echo get_field( 'bathrooms' )?> Baths</span>
				<span class="garage"><i class="icon garage"></i><?php echo get_field( 'garages' )?> Garage(s)</span>
			</p>
			<p class="price"><?php echo get_field( 'price' )?></p>
		</section>
		<div class="separator"></div>
		<?php
		   $gallery = get_field( 'gallery' );
           
           if( $gallery ):
		?>
		<h2>Photo gallery</h2>
		<div class="gallery"><?php 
		  echo $gallery;
          egs_display();
		?></div>
		<div class="separator"></div>
		<?php endif; ?>
		<section class="details clearfix">
			<div class="description">
				<h3>Description</h3>
				<?php echo get_field( 'description' )?>
			</div>
			<div class="features">
				<h3>Features</h3>
				<?php echo get_field( 'features' ) ?>
			</div>
		</section>
	</div>
	<?php
	   $latitude = get_field( 'gps_coordinates_lat' );
       $longitude = get_field( 'gps_coordinates_long' );
       
       if( isset($latitude) && isset($longitude) ) {
           if ( $latitude != 0 && $longitude != 0 ) :
    ?>
       <script>
            var latitude = <?php echo $latitude; ?>;
            var longitude = <?php echo $longitude; ?>;
       </script>
    <?php 
        endif;
       }
	?>
	<div id="map_canvas"></div>
</article>