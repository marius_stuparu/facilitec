<article class="clearfix">
	<div class="content">
		<section>
			<?php 
			the_post();
			the_content();
			?>
		</section>
	</div>
</article>