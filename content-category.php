<?php
$count = 0;
   
while (have_posts()) : the_post();
    $count++;
    global $post;
    
    $image = get_field('main_image');
    $price = get_field('price');
?>
<article id="property-<?php the_ID() ?>" class="property">
	<div class="content">
		<section class="numbers clearfix">
			<h1>
				<a href="<?php echo get_permalink() ?>"><?php echo get_field( 'name' ) ?></a>
				<br /><small><?php echo get_field( 'location' )?></small>
			</h1>
			<p class="figures">
				<span class="surface"><i class="icon surface"></i><?php echo get_field( 'surface' )?> m<sup>2</sup></span>
				<span class="beds"><i class="icon beds"></i><?php echo get_field( 'bedrooms' )?> Beds</span>
				<span class="baths"><i class="icon baths"></i><?php echo get_field( 'bathrooms' )?> Baths</span>
				<span class="garage"><i class="icon garage"></i><?php echo get_field( 'garages' )?> Garage(s)</span>
			</p>
		</section>
		<section class="details clearfix">
		    <div class="image">
		        <?php if ( isset( $image ) ) : ?>
		        <img alt="<?php echo get_field( 'name' ) ?>" src="<?php echo $image ?>" />
		        <?php endif; ?>
		        <div class="price">
		            <p class="the_price"><?php echo $price ?></p>
		            <p class="link"><a href="<?php echo get_permalink() ?>">more info<span class="arrow"></span></a></p>
		        </div>
		    </div>
		</section>
	</div>
</article>
<?php endwhile; ?>