<article>
	<div class="content clearfix">
	    <?php the_post() ?>
	    <h1><?php the_title() ?></h1>
        <?php
            wp_nav_menu( array(
                'theme_location' => 'services',
                'container' => 'div',
                'container_id' => 'services_menu'
            ) );
            
        ?>
        <div class="separator"></div>
        <?php the_content(); ?>
	</div>
</article>