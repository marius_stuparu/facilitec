<div id="sidebar" class="col-right">

	<?php if (is_active_sidebar('primary')) : ?>
    <div class="primary">
		<?php dynamic_sidebar('primary'); ?>		           
	</div>        
	<?php endif; ?>

	<?php if (is_active_sidebar('secondary-1') || 
			  is_active_sidebar('secondary-2') ) : ?>
    <div class="secondary">
		<?php dynamic_sidebar('secondary-1'); ?>		           
	</div>        
    <div class="secondary last">
		<?php dynamic_sidebar('secondary-2'); ?>		           
	</div>        
	<?php endif; ?>
    
	
</div><!-- /#sidebar -->