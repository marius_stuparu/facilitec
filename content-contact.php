<article>
    <div id="map_canvas"></div>
	<div class="content">
		<section class="numbers clearfix">
			<?php
			 the_post();
             the_content();
			?>
		</section>
	</div>
	<script>
	    var latitude = 44.446865;
	    var longitude = 26.096844;
	</script>
</article>