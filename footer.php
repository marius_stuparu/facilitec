<?php
/**
 * The template for displaying the footer.
 *
 * @package facilitec
 */
?>
<footer>
    <div class="content">
        <?php
            wp_nav_menu( array(
                'theme_location' => 'footer',
                'container' => 'div',
            ) );
        ?>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>