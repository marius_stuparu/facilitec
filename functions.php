<?php
/**
 * facilitec functions and definitions
 *
 * @package facilitec
 */
/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );
/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter( 'ot_show_new_layout', '__return_false' );
/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );
/**
 * Required: include OptionTree.
 */
include_once( 'option-tree/ot-loader.php' );
include_once( 'theme-options.php' );
 
/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
    $content_width = 1000;
/** Tell WordPress to run facilitec_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'facilitec_setup' );
if ( ! function_exists( 'facilitec_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override facilitec_setup() in a child theme, add your own facilitec_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support() To add support for post thumbnails, custom headers and backgrounds, and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 */
function facilitec_setup() {
    // This theme styles the visual editor with editor-style.css to match the theme style.
    //add_editor_style();
    // This theme uses post thumbnails
    add_theme_support( 'post-thumbnails' );
    //add_theme_support( 'post-formats', array( 'gallery', 'image', 'video', 'audio' ) );
    add_theme_support( 'automatic-feed-links' );
    // Make theme available for translation
    // Translations can be filed in the /languages/ directory
    //load_theme_textdomain( 'facilitec', get_template_directory() . '/languages' );
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => __( 'Main menu', 'facilitec' ),
        'footer' => __( 'Footer menu', 'facilitec' ),
        'services' => __( 'Services', 'facilitec' ),
    ) );
    set_post_thumbnail_size( 133, 99 );
    add_image_size( 'home_slider_large', 1920, 751 );
}
endif;
/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
function facilitec_page_menu_args( $args ) {
    $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'facilitec_page_menu_args' );
/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @return int
 */
function facilitec_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'facilitec_excerpt_length' );
/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @return string "Continue Reading" link
 */
function facilitec_continue_reading_link() {
    return ' <a href="'. get_permalink() . '" class="more-link"">' . __( 'Continuarea', 'facilitec' ) . '</a>';
}
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and facilitec_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @return string An ellipsis
 */
function facilitec_auto_excerpt_more( $more ) {
    return ' &hellip;' . facilitec_continue_reading_link();
}
add_filter( 'excerpt_more', 'facilitec_auto_excerpt_more' );
/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function facilitec_custom_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= facilitec_continue_reading_link();
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'facilitec_custom_excerpt_more' );
/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override facilitec_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function facilitec_widgets_init() {
    // Sidebar pagini
    register_sidebar( array(
        'name' => __( 'Sidebar pagini', 'facilitec' ),
        'id' => 'sidebar-1',
        'description' => __( 'Sidebar-ul de langa paginile statice', 'facilitec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    
    // Sidebar pagini
    register_sidebar( array(
        'name' => __( 'Sidebar articole', 'facilitec' ),
        'id' => 'sidebar-2',
        'description' => __( 'Sidebar-ul de langa articolele de blog', 'facilitec' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
/** Register sidebars by running facilitec_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'facilitec_widgets_init' );
if ( ! function_exists( 'facilitec_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function facilitec_content_nav( $nav_id ) {
    global $wp_query;
    if ( $wp_query->max_num_pages > 1 ) :
?>
<div id="<?php echo $nav_id; ?>">
<div class="nav-older"><?php next_posts_link(__('Mai vechi', 'facilitec')); ?><
/div>
<div class="nav-newer"><?php previous_posts_link(__('Mai recente', 'facilitec')); ?><
/div>
</div><!-- page nav -->
<?php endif;
	}
	endif;
	/**
	* Removes the default styles that are packaged with the Recent Comments widget.
	*
	* To override this in a child theme, remove the filter and optionally add your own
	* function tied to the widgets_init action hook.
	*
	* This function uses a filter (show_recent_comments_widget_style) new in WordPress 3.1
	* to remove the default style.
	*/
	function facilitec_remove_recent_comments_style() {
	add_filter( 'show_recent_comments_widget_style', '__return_false' );
	}
	add_action( 'widgets_init', 'facilitec_remove_recent_comments_style' );
	if ( ! function_exists( 'facilitec_posted_on' ) ) :
	/**
	* Prints HTML with meta information for the current post-date/time and author.
	*/
	function facilitec_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'facilitec' ),
	'meta-prep meta-prep-author',
	sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
	get_permalink(),
	esc_attr( get_the_time() ),
	get_the_date()
	),
	sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
	get_author_posts_url( get_the_author_meta( 'ID' ) ),
	esc_attr( sprintf( __( 'View all posts by %s', 'facilitec' ), get_the_author() ) ),
	get_the_author()
	)
	);
	}
	endif;
	if ( ! function_exists( 'facilitec_posted_in' ) ) :
	/**
	* Prints HTML with meta information for the current post (category, tags and permalink).
	*/
	function facilitec_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
	$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'facilitec' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
	$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'facilitec' );
	} else {
	$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'facilitec' );
	}
	// Prints the string, replacing the placeholders.
	printf(
	$posted_in,
	get_the_category_list( ', ' ),
	$tag_list,
	get_permalink(),
	the_title_attribute( 'echo=0' )
	);
	}
	endif;
	// Remove the WordPress Generator
	function change_generator() { return '<meta name="generator" content="Marius Stuparu http://www.mariusstuparu.com" />'; }
	add_filter('the_generator','change_generator');
	function facilitec_body_classes( $classes ) {
	if ( is_singular() && ! is_home() )
	$classes[] = 'singular';
	if ( is_page( 'team' ) || is_page( 9 ) ) {
	$classes[] = 'team';
	}
	return $classes;
	}
	add_filter( 'body_class', 'facilitec_body_classes' );
	// Enqueue scripts
	function facilitec_register_scripts() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', null, '1.8.1', true);
	wp_register_script( 'plugins', get_bloginfo( 'template_url' ) . '/js/plugins.js', array('jquery'), '1', true);
	wp_register_script( 'main', get_bloginfo( 'template_url' ) . '/js/main.js', array('jquery', 'plugins'), '1', true);
	wp_register_script( 'slider', get_bloginfo( 'template_url' ) . '/js/vendor/jquery.nivo.slider.pack.js', array('jquery',), '1', true);
	wp_register_script( 'waypoints', get_bloginfo( 'template_url' ) . '/js/vendor/waypoints.min.js', array('jquery',), '1', true);
	wp_register_script( 'googlemaps', 'http://maps.google.com/maps/api/js?sensor=false', null, '1', false);
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'waypoints' );
	wp_enqueue_script( 'main' );

	if( is_home() ) {
	wp_enqueue_script( 'slider' );
	}

	if ( is_single() || is_page(2) ) {
	wp_enqueue_script( 'googlemaps' );
	}
	}
	add_action('wp_enqueue_scripts', 'facilitec_register_scripts');
	function facilitec_copyright_notice(){
	$current_date = getdate();
	$current_year = $current_date['year'];
	$copyright = 'Copyrigh &copy; ';
	if ($current_year == 2012){
	$copyright .= '2012' . ' | ' .get_bloginfo('name') . '. Toate drepturile rezervate.';
	} else {
	$copyright .= '2012-' . $current_year . ' | ' .get_bloginfo('name') . '. Toate drepturile rezervate.';
	}
	return $copyright;
	}
	/**
	* @package     TGM-Plugin-Activation
	* @version     2.3.6
	* @author      Thomas Griffin <thomas@thomasgriffinmedia.com>
	* @author      Gary Jones <gamajo@gamajo.com>
	* @copyright  Copyright (c) 2012, Thomas Griffin
	* @license     http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
	* @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
	*/
	/**
	* Include the TGM_Plugin_Activation class.
	*/
	require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
	add_action( 'tgmpa_register', 'register_required_plugins' );
	/**
	* Register the required plugins for this theme.
	*
	* The variable passed to tgmpa_register_plugins() should be an array of plugin
	* arrays.
	*
	* This function is hooked into tgmpa_init, which is fired within the
	* TGM_Plugin_Activation class constructor.
	*/
	function register_required_plugins() {
	/**
	* Array of plugin arrays. Required keys are name and slug.
	* If the source is NOT from the .org repo, then source is also required.
	*/
	$plugins = array(
	array(
	'name'      => 'Advanced Custom Fields',
	'slug'      => 'advanced-custom-fields',
	'required'  => true,
	),
	array(
	'name'      => 'Auto Post Thumbnail',
	'slug'      => 'auto-post-thumbnail',
	'required'  => true,
	),
	array(
	'name'      => 'Antispam Bee',
	'slug'      => 'antispam-bee',
	'required'  => false,
	),
	array(
	'name'      => 'Bad Behavior',
	'slug'      => 'bad-behavior',
	'required'  => true,
	),
	array(
	'name'      => 'BBQ: Block Bad Queries',
	'slug'      => 'block-bad-queries',
	'required'  => true,
	),
	array(
	'name'      => 'Email Encoder Bundle',
	'slug'      => 'email-encoder-bundle',
	'required'  => false,
	),
	array(
	'name'      => 'Google Analytics for WordPress',
	'slug'      => 'google-analytics-for-wordpress',
	'required'  => false,
	),
	array(
	'name'      => 'Google XML Sitemaps',
	'slug'      => 'google-sitemap-generator',
	'required'  => false,
	),
	array(
	'name'      => 'Relevanssi',
	'slug'      => 'relevanssi',
	'required'  => false,
	),
	array(
	'name'      => 'OSE Firewall™',
	'slug'      => 'ose-firewall',
	'required'  => true,
	),
	array(
	'name'      => 'WordPress SEO by Yoast',
	'slug'      => 'wordpress-seo',
	'required'  => true,
	),
	array(
	'name'      => 'WP-Optimize',
	'slug'      => 'wp-optimize',
	'required'  => false,
	),
	);

	// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'facilitec';
	/**
	* Array of configuration settings. Amend each line as needed.
	* If you want the default strings to be available under your own theme domain,
	* leave the strings uncommented.
	* Some of the strings are added into a sprintf, so see the comments at the
	* end of each line for what each argument will be.
	*/
	$config = array(
	'domain'            => $theme_text_domain,          // Text domain - likely want to be the same as your theme.
	'default_path'      => '',                          // Default absolute path to pre-packaged plugins
	'parent_menu_slug'  => 'themes.php',                // Default parent menu slug
	'parent_url_slug'   => 'themes.php',                // Default parent URL slug
	'menu'              => 'install-required-plugins',  // Menu slug
	'has_notices'       => true,                        // Show admin notices or not
	'is_automatic'      => true,                        // Automatically activate plugins after installation or not
	'message'           => '',                          // Message to output right before the plugins table
	'strings'           => array(
	'page_title'                                => __( 'Install Required Plugins', $theme_text_domain ),
	'menu_title'                                => __( 'Install Plugins', $theme_text_domain ),
	'installing'                                => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
	'oops'                                      => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
	'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
	'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
	'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
	'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
	'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
	'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
	'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
	'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
	'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
	'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
	'return'                                    => __( 'Return to Required Plugins Installer', $theme_text_domain ),
	'plugin_activated'                          => __( 'Plugin activated successfully.', $theme_text_domain ),
	'complete'                                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
	'nag_type'                                  => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
	)
	);
	tgmpa( $plugins, $config );
	}

	function team_members() {
		$labels = array(
			'name'               => _x( 'Team', 'post type general name' ),
			'add_new'            => _x( 'Add New', 'book' ),
			'add_new_item'       => __( 'Add New Member' ),
			'edit_item'          => __( 'Edit Member' ),
			'new_item'           => __( 'New Member' ),
			'all_items'          => __( 'All Members' ),
			'view_item'          => __( 'View Member' ),
			'search_items'       => __( 'Search Members' ),
			'not_found'          => __( 'No team members found' ),
			'not_found_in_trash' => __( 'No team members found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => 'Team'
		);
		$args = array(
			'labels'        => $labels,
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array( 'custom-fields', 'thumbnail', 'revisions', 'title', 'page-attributes' ),
			'has_archive'   => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'rewrite' => true,
			'capability_type' => 'page',
			'has_archive' => true,
			'hierarchical' => false,
			'query_var' => true,
		);
		register_post_type( 'team', $args );
	}
	add_action( 'init', 'team_members' );
	
	function clients() {
		$labels = array(
			'name'               => _x( 'Clients', 'post type general name' ),
			'add_new'            => _x( 'Add New', 'book' ),
			'add_new_item'       => __( 'Add New Client' ),
			'edit_item'          => __( 'Edit Client' ),
			'new_item'           => __( 'New Client' ),
			'all_items'          => __( 'All Clients' ),
			'view_item'          => __( 'View Client' ),
			'search_items'       => __( 'Search Clients' ),
			'not_found'          => __( 'No clients found' ),
			'not_found_in_trash' => __( 'No clients found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => 'Clients'
		);
		$args = array(
			'labels'        => $labels,
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array( 'custom-fields', 'thumbnail', 'revisions', 'title', 'page-attributes' ),
			'has_archive'   => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'rewrite' => true,
			'capability_type' => 'page',
			'has_archive' => true,
			'hierarchical' => false,
			'query_var' => true,
		);
		register_post_type( 'client', $args );
	}
	add_action( 'init', 'clients' );

// Remove un-needed menu items
function remove_menu_items() {
  global $menu;
  $restricted = array(
    __('Links'),
    __('Comments'),
  );
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
      unset($menu[key($menu)]);}
    }
  }

add_action('admin_menu', 'remove_menu_items');

// Rename Posts
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Properties';
    $submenu['edit.php'][5][0] = 'Properties';
    $submenu['edit.php'][10][0] = 'Add Property';
    $submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    $submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    echo '';
}

function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Properties';
        $labels->singular_name = 'Property';
        $labels->add_new = 'Add Property';
        $labels->add_new_item = 'Add Property';
        $labels->edit_item = 'Edit Properties';
        $labels->new_item = 'Property';
        $labels->view_item = 'View Property';
        $labels->search_items = 'Search Properties';
        $labels->not_found = 'No Properties found';
        $labels->not_found_in_trash = 'No Properties found in Trash';
    }
    add_action( 'init', 'change_post_object_label' );
    add_action( 'admin_menu', 'change_post_menu_label' );